import tweepy
import time
import datetime
from datetime import date
import csv
from tweepy.streaming import StreamListener

# set your user key and token
consumer_key = 'xxxxx'
consumer_secret = 'xxxxx'
token = 'xxxxx'
access_token = 'xxxxx'
access_token_secret = 'xxxxx'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=False)

# Method to format a tweet from tweepy
def reformat_tweet(tw):
    x = tw
    processed_doc = {
        "id": x["id"],
        "lang": x["lang"],
        "retweeted_id": x["retweeted_status"]["id"] if "retweeted_status" in x else None,
        "favorite_count": x["favorite_count"] if "favorite_count" in x else 0,
        "retweet_count": x["retweet_count"] if "retweet_count" in x else 0,
        "coordinates_latitude": x["coordinates"]["coordinates"][0] if x["coordinates"] else 0,
        "coordinates_longitude": x["coordinates"]["coordinates"][0] if x["coordinates"] else 0,
        "place": x["place"]["country_code"] if x["place"] else None,
        "user_id": x["user"]["id"],
        "created_at": time.mktime(time.strptime(x["created_at"], "%a %b %d %H:%M:%S +0000 %Y"))
    }

    if x["entities"]["hashtags"]:
        processed_doc["hashtags"] = [{"text": y["text"], "startindex": y["indices"][0]} for y in
                                        x["entities"]["hashtags"]]
    else:
        processed_doc["hashtags"] = []

    if x["entities"]["user_mentions"]:
        processed_doc["usermentions"] = [{"screen_name": y["screen_name"], "startindex": y["indices"][0]} for y in
                                            x["entities"]["user_mentions"]]
    else:
        processed_doc["usermentions"] = []

    if "extended_tweet" in x:
        processed_doc["text"] = x["extended_tweet"]["full_text"]
    elif "full_text" in x:
        processed_doc["text"] = x["full_text"]
    else:
        processed_doc["text"] = x["text"]

    return processed_doc


# Open/Create a file to append data
lang = 'en'
filename = date.today()
csvFile = open(f'../data/raw/{lang}-tweets-{filename}.csv', 'a')
# Use csv Writer
csvWriter = csv.writer(csvFile)


def write_to_csv(data):
    if 'RT @' not in data['text']:
        csvWriter.writerow([datetime.datetime.fromtimestamp(data["created_at"]).strftime('%Y-%m-%d %H:%M:%S'), data['retweet_count'], data['text']])


for tweet in tweepy.Cursor(api.search, q="unemployment", lang=lang, count="10000", since="2020-03-01").items():
    print(reformat_tweet(tweet._json))
    write_to_csv(reformat_tweet(tweet._json))


