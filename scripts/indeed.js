var url = 'https://fr.indeed.com/France-Emplois';

var Nightmare = require('nightmare');
var nightmare = Nightmare({
    openDevTools: true,
    show: true,
    fullscreen: false})
    .useragent('Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');

var MongoClient = require('mongodb').MongoClient;
var host = 'mongodb://localhost/interim';

MongoClient.connect(host, function(error, db) {
    if(error) {
        console.log('Connection mongoDB KO : ', err);
    }else {
        console.log('Connection mongoDB OK : ', host);

        var getPage = function() {
            nightmare
                .inject('js', '../node_modules/jquery/dist/jquery.js')
                .evaluate(function() {

                    var extraction = [];

                    jQuery("td#resultsCol > div.jobsearch-SerpJobCard").each(
                        function(index) {

                            var titre = jQuery(this).find("h2.title > a").text().trim();
                            var entreprise = jQuery(this).find("div > div > span.company > a").text().trim();
                            var localisation = jQuery(this).find("div > div.location.accessible-contrast-color-location").text().trim();
                            var date_raw = jQuery(this).find("div > div > div > div > span.date").text().trim();
                            var date = new Date();
                                                     
                            if(date_raw == "Publiée à l'instant" || date_raw == "Aujourd'hui"){
                                var date_acc = '1';
                                var date = date;
                            } else if(date_raw == "Il y a plus de 30 jours") { 
                                var date_acc = '0';
                                var date = new Date(date.getDate()-30);
                            } else {    
                                var jours = date_raw.match(/\d+/)[0];
                                var date_acc = '1';
                                var date = new Date(date.setDate(date.getDate() - jours));
                            }


                        if(localisation.includes("(")){
                            var local = localisation.split("(");
                            var ville = local[0].trim();
                            var region_code = local[1].replace(")", "").trim();
                        } else {
                            var ville = localisation;
                            var region_code = "";
                        }

                            extraction.push(
                                {
                                    titre : titre,
                                    entreprise : entreprise,
                                    localisation : localisation,
                                    region_code : region_code,
                                    ville : ville,
                                    date : date,
                                    date_acc : date_acc
                                })
                        }
                    );
                    return extraction;
                })
                .then(
                function(result) {

                    console.log(result);

                    if (nightmare.exists("div.pagination > ul.pagination-list > li:last-child > a")) {
                        nightmare.click("div.pagination > ul.pagination-list > li:last-child > a");
                        nightmare.wait(1500).then(function(){
                            getPage()
                        })
                    }
                    db.collection('indeed').insertMany(result)
                },
                function(err) {

                    console.error('error', err);

                    db.close();
                    nightmare.end()
                        .then()
                }
            );
        };

        nightmare
            .goto(url)
            .wait(1500)
            .then(
            function() {
                getPage();
            }
        );
    }
});