var url = 'https://candidat.pole-emploi.fr/offres/recherche?lieux=01P&offresPartenaires=true&range=0-19&rayon=10&tri=0';

var Nightmare = require('nightmare');
var nightmare = Nightmare({
    openDevTools: true,
    show: true,
    fullscreen: false})
    .useragent('Chrome/38.0.2125.111 Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36');
    //Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36 

var MongoClient = require('mongodb').MongoClient;
var host = 'mongodb://localhost/interim';

MongoClient.connect(host, function(error, db) {
    if(error) {
        console.log('Connection mongoDB KO : ', err);
    } else {
        console.log('Connection mongoDB OK : ', host);

        var clickClick = function(result){
        	var i = 0
            while (i < 35) {
                if(nightmare.exists("div#zoneAfficherPlus > p > a")){
                    nightmare.click("div#zoneAfficherPlus > p > a");
                }
                nightmare.wait(2000)                
                i++;
            }            			
        }

        var getPage = function() {
            nightmare
                .inject('js', '../node_modules/jquery/dist/jquery.js')                             
                .evaluate(function() {

                    var extraction = [];

                    jQuery("div.results > div#zoneAfficherListeOffres > ul > li").each(
                        function(index) {

                            var titre = jQuery(this).find("a > div.media-body > h2").text().trim();
                            var entreprise_raw = jQuery(this).find("a > div.media-body > p.subtext").text().trim();  

                            if(entreprise_raw.includes(" - ")){
                                var pos = entreprise_raw.search(/\n/);
                                var entreprise = entreprise_raw.substr(0, pos)
                            } else {
                                var entreprise = entreprise_raw;
                            }

                            var localisation = jQuery(this).find("a > div.media-body > p.subtext > span").text().trim();
                            var date_raw = jQuery(this).find("a > div.media-body > p.date").text().trim();
                            var date = new Date();
                                                     
                            if(date_raw == "Publié aujourd'hui"){
                                var date_acc = '1';
                                var date = date;
                            } else if(date_raw == "Publié hier") { 
                                var date_acc = '1';
                                var date = new Date(date.getDate()-1);
                            } else {    
                                var jours = date_raw.match(/\d+/)[0];
                                var date_acc = '1';
                                var date = new Date(date.setDate(date.getDate() - jours));
                            }

                        if(localisation.includes(" - ")){
                            var local = localisation.split(" - ");
                            var ville = local[1].trim();
                            var region_code = local[0].trim();
                        } else {
                            var ville = localisation;
                            var region_code = "";
                        }

                            extraction.push(
                                {
                                    titre : titre,
                                    entreprise : entreprise,
                                    localisation : localisation,
                                    region_code : region_code,
                                    ville : ville,
                                    date : date,
                                    date_acc : date_acc
                                })
                        }
                    );
                    return extraction;
                })
                .then(
                function(result) {                   

                    console.log(result);

                    db.collection('pole_emploi').insertMany(result)
                },
                function(err) {

                    console.error('error', err);

                    db.close();
                    nightmare.end()
                        .then()
                }
            );
        };

        nightmare
            .goto(url)
            .wait(1500)
            .then(
            	function() {
            		clickClick();
            })
            .then(
            	function() {
                	getPage();
            }
        );
    }
});